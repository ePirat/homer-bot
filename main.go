package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

const (
	tokenEnvVar       = "GITLAB_PRIVATE_TOKEN"
	rootURLEnvVar     = "GITLAB_API_ROOT_URL"
	defaultAPIRootURL = "https://code.videolan.org/api/v4"
	acceptedLabel     = "Accepted"
)

var (
	verbose                = false
	debug                  = false
	mrInactiveHours        int64
	mrDevScoreHours        int64
	mrOtherScoreHours      int64
	mrDevDiscussionHours   int64
	mrOtherDiscussionHours int64
)

func logMR(mrID int64, v ...interface{}) {
	log.Println(fmt.Sprintf("[MR %d]", mrID), fmt.Sprint(v...))
}
func logMRVerbose(mrID int64, v ...interface{}) {
	if verbose || debug {
		logMR(mrID, v...)
	}
}

// Markdown report
func generateReport(mr MergeRequest, checks []Check) string {
	fullSuccess := true
	report := fmt.Sprintln("## MR acceptance report")
	for _, check := range checks {
		report += fmt.Sprintln("* ", check.Markdown())
		if !check.Success {
			fullSuccess = false
		}
	}
	report += fmt.Sprintln("## Conclusion")
	if fullSuccess {
		report += fmt.Sprintln("I am Homer and I approve this MergeRequest.")
	} else {
		report += fmt.Sprintln("D'oh!")
	}
	return report
}

func main() {
	var projectID int64
	var dryrun bool
	flag.Int64Var(&projectID, "project", 0, "The Gitlab Project ID. Look at the Project Home Page")
	flag.BoolVar(&verbose, "verbose", false, "Verbose mode.")
	flag.BoolVar(&debug, "debug", false, "Debug mode. Shows HTTP requests")
	flag.BoolVar(&dryrun, "dryrun", false, "Do not touch anything - only analyze merge requests.")
	// Timeout/duration parameters for checks
	flag.Int64Var(&mrInactiveHours, "mr-inactive-hours", 72, "hours before accepting an inactive developer MR")
	flag.Int64Var(&mrDevScoreHours, "mr-devscore-hours", 24, "hours before considering a >=0 vote score for a developer MR is OK")
	flag.Int64Var(&mrOtherScoreHours, "mr-otherscore-hours", 72, "hours before considering a >0 vote score for a external participant MR is OK")
	flag.Int64Var(&mrDevDiscussionHours, "mr-devdiscussion-hours", 24, "hours before considering resolved threads for a developer MR are OK")
	flag.Int64Var(&mrOtherDiscussionHours, "mr-otherdiscussion-hours", 72, "hours before considering resolved threads for an external participant MR are OK")

	flag.Parse()
	if projectID == 0 {
		log.Println("Please provide a Project ID.")
		os.Exit(-1)
	}
	token := os.Getenv(tokenEnvVar)
	if token == "" {
		log.Println(tokenEnvVar, "environment variable not set! Aborting...")
		os.Exit(-1)
	}
	rootURL := os.Getenv(rootURLEnvVar)
	if rootURL == "" {
		rootURL = defaultAPIRootURL
	}
	project := Project{ID: projectID, APIRootURL: rootURL, AuthToken: token}
	mrs, err := project.GetMRs(map[string]string{"state": "opened", "wip": "no"})
	if err != nil {
		log.Println("Error while retrieving Project's MRs: ", err.Error())
		os.Exit(-1)
	}
	log.Println("In Project Number ", project.ID)
	numberOfMRs := len(mrs)
	errors := make(chan error, numberOfMRs)
	for _, mr := range mrs {
		go func(mr MergeRequest) {
			if mr.WorkInProgress {
				logMR(mr.IID, "MR ", mr.IID, " is flagged as Work in Progress, ignoring it")
				errors <- nil
				return
			}
			logMR(mr.IID, "Active MR:", mr.IID)
			checks := mr.Analyze()
			fullSuccess := true
			var failedCheck Check
			var checkError error = nil
			for _, check := range checks {
				if !check.Success {
					fullSuccess = false
					failedCheck = check
				}
				if check.Error != nil {
					checkError = check.Error
				}
				logMRVerbose(mr.IID, check.String())
			}
			if checkError != nil {
				logMR(mr.IID, "ERROR! Something gone wrong during the MR analyze: ", checkError.Error())
				// Do not perform any operation if an error occured
				errors <- checkError
				return
			} else if fullSuccess {
				logMR(mr.IID, "SUCCESS! MergeRequest is fully compliant!")
			} else {
				logMR(mr.IID, "FAIL! MergeRequest is not fully compliant: ", failedCheck.String())
			}
			if fullSuccess && !mr.HasLabel(acceptedLabel) {
				logMR(mr.IID, "MergeRequest is fully compliant and has no Accept Label! We should do this!")
				if dryrun {
					logMRVerbose(mr.IID, "Bypassing Label addition (dryrun)")
				} else {
					if err := mr.AddLabel(acceptedLabel); err != nil {
						logMR(mr.IID, "ERROR while trying to add a label in MR:", err)
					} else {
						logMR(mr.IID, "Label successfully added")
					}
					if err := mr.CreateNote(generateReport(mr, checks)); err != nil {
						logMR(mr.IID, "ERROR while trying to add a comment in MR:", err)
					} else {
						logMR(mr.IID, "Comment successfully added")
					}

				}
			} else if !fullSuccess && mr.HasLabel(acceptedLabel) {
				logMR(mr.IID, "MergeRequest does not meet the requirements, and has the Accept Label! We should remove it.")
				if dryrun {
					logMRVerbose(mr.IID, "Bypassing Label deletion (dryrun)")
				} else {
					if err := mr.RemoveLabel(acceptedLabel); err != nil {
						logMR(mr.IID, "ERROR while trying to remove a label in MR:", err)
					} else {
						logMR(mr.IID, "Label successfully removed")
					}
					if err := mr.CreateNote(generateReport(mr, checks)); err != nil {
						logMR(mr.IID, "ERROR while trying to add a comment in MR:", err)
					} else {
						logMR(mr.IID, "Comment successfully added")
					}
				}
			}
			errors <- nil
		}(mr)
	}
	var finalError error = nil
	for i := 0; i < numberOfMRs; i++ {
		if err := <-errors; err != nil {
			finalError = err
		}
	}
	if finalError != nil {
		log.Println("An error has occured during the process!")
		os.Exit(-2)
	} else {
		log.Println("All MRs have been processed.")
		os.Exit(0)
	}
}
