package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

func logDebug(v ...interface{}) {
	if debug {
		log.Println(v...)
	}
}

// A simple HTTP request.
// Takes a HTTP method, a base URL, url PARAMS, a body to push (for PUT, POST, etc. methods), and a HTTP Headers map.
// Returns a status code, a body as byte array, HTTP response headers, and an error if something gone wrong
// Of course, you should always check the error value.
func request(method string, baseURL string, urlParams map[string]string, requestBody []byte, headersMap map[string]string) (int, []byte, http.Header, error) {
	fullURL := baseURL
	if len(urlParams) > 0 {
		v := url.Values{}
		for key, value := range urlParams {
			v.Add(key, value)
		}
		fullURL = fullURL + "?" + v.Encode()
	}
	client := &http.Client{}
	logDebug("REQUEST", method, fullURL)
	req, err := http.NewRequest(method, fullURL, bytes.NewReader(requestBody))
	if err != nil {
		return 0, []byte{}, nil, err
	}
	for key, value := range headersMap {
		req.Header.Set(key, value)
	}
	resp, err := client.Do(req)
	if err != nil {
		return 0, []byte{}, nil, err
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, []byte{}, nil, err
	}
	logDebug("RESPONSE", method, fullURL, resp.StatusCode, string(respBody))
	return resp.StatusCode, respBody, resp.Header, nil
}

func requestJSON(method string, url string, urlParams map[string]string, reqJSON interface{}, token string, bindingObject interface{}) error {
	var body []byte
	if reqJSON != nil {
		var err error
		body, err = json.Marshal(reqJSON)
		if err != nil {
			return err
		}
	}
	code, responseBody, _, err := request(method, url, urlParams, body, map[string]string{"Private-Token": token, "Content-Type": "application/json"})
	if err != nil {
		return err
	}
	if (method == http.MethodPost && code != http.StatusCreated) || (method != http.MethodPost && code != http.StatusOK) {
		return fmt.Errorf("Request %s %s returned an HTTP Status Error: %d - %s", method, url, code, responseBody)
	}
	if bindingObject != nil {
		if err := json.Unmarshal(responseBody, bindingObject); err != nil {
			return err
		}
	}
	return nil
}
